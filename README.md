# Makros für Einzel-Ranglistenmatches mit Google Sheets

Fügt dem Menü in Google Sheets den Punkt `Functions` hinzu. Darunter kommen zwei Funktionen:

* `Runde eintragen`

    Resultate einer Runde eintragen

* `Spieler hinzufügen`

    Fügt einen Spieler der Rangliste hinzu

Es werden jeweils Info-Boxen mit/ohne Aufforderung zur Eingabe eines Text geöffnet, die durch die einzelnen Schritte leiten.