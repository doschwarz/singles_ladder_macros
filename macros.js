// TODO: Write recalculate function based on the second and third sheet
// TODO: Maybe separate points calculation into function

function onOpen() {
  const spreadsheet = SpreadsheetApp.getActive();
  const menuItems = [
    {name: 'Runde eintragen', functionName: 'enterNewRound'},
    {name: 'Spieler hinzufügen', functionName: 'addPlayer'}
  ];
  spreadsheet.addMenu('Functions', menuItems);
}

function determineRowFirstPlayer(tableSheet) {
  for (i = 1; i <= tableSheet.getLastRow(); i++) {
    if (tableSheet.getRange(i, 1).getValue() == 1) {
      return i;
    }
  }
}

function addPlayer(name) {
  var sort = true;
  const fromEnterRound = Boolean(name);
  if (!fromEnterRound) {
    const input1 = Browser.inputBox('Name:', Browser.Buttons.OK_CANCEL);
    if (input1 == 'cancel') {
      return;
    }
    name = String(input1);
  }
  const tableSheet = SpreadsheetApp.getActive().getSheetByName('Tabelle');
  const lastRow = tableSheet.getLastRow();
  const rowFirstPlayer = determineRowFirstPlayer(tableSheet);
  const table = tableSheet.getRange(rowFirstPlayer, 1, lastRow - rowFirstPlayer + 1, 3).getValues();
  
  if(fromEnterRound) {
    var playerList = [];
    for (i = 0; i < table.length; i++) {
      playerList.push(table[i][1]);
    }
    if (playerList.indexOf(name) != -1) {
      Browser.msgBox('Fehler: ' + name + ' ist schon in der Rangliste aufgeführt.');
      return;
    }
  }
  
  const input2 = Browser.msgBox('Soll ' + name + ' auf dem letzten Rang der Tabelle starten?', Browser.Buttons.YES_NO);
  var points;
  
  if (input2 == 'yes') {
    points = table[lastRow - rowFirstPlayer][2] - 1;
    sort = false;
  }
  else {
    const input3 = Browser.inputBox('Anfangsrang von ' + name + ':', Browser.Buttons.OK_CANCEL);
    if (input3 == 'cancel'){
      return;
    }
    
    const initialRank = parseInt(input3);
    if (initialRank < 1) {
      Browser.msgBox(initialRank + ' ist kein gültiger Anfangsrang. Breche ab.')
      return;
    }
    if (initialRank == 1) {
      points = table[0][2] + 1;
    }
    else if(initialRank + rowFirstPlayer > lastRow) {
      if(initialRank + rowFirstPlayer > lastRow + 1) {
        Browser.msgBox('Gewählter Anfangsrang ist zu hoch. Setze den neuen Spieler ans Ende der Tabelle.')
      }
      points = table[lastRow - rowFirstPlayer][2] - 1;
      sort = false;
    }
    else {
      points = Math.floor((table[initialRank - 2][2] + table[initialRank - 1][2]) / 2);
    }
  }
  
  tableSheet.appendRow([table[lastRow - rowFirstPlayer][0] + 1, name, points]);
  tableSheet.getRange(lastRow + 1, 1, 1, 3).setHorizontalAlignment("left");
  
  if(sort) {
    updateTable(tableSheet, rowFirstPlayer);
  }
  
  const historySheet = SpreadsheetApp.getActive().getSheetByName('History');
  historySheet.appendRow([name, Utilities.formatDate(new Date(), "GMT", "dd.MM.yy"), points]);
  historySheet.getRange(historySheet.getLastRow(), 1, 1, 3).setHorizontalAlignment("left");
  
  return 1;
}

function updateTable(tableSheet, rowFirstPlayer) {
  const lastRow = tableSheet.getLastRow();
  tableSheet.getRange(rowFirstPlayer, 2, lastRow - rowFirstPlayer + 1, 2).sort({column: 3, ascending: false});
  
  var table = tableSheet.getRange(rowFirstPlayer, 1, lastRow - rowFirstPlayer + 1, 3).getValues();
  table[0][0] = 1;
  for (i = 1; i < table.length; i++) {
    table[i][0] = i + 1;
    if (table[i-1][2] == table[i][2]) {
      table[i][0] = table[i-1][0];
    }
    tableSheet.getRange(rowFirstPlayer + i, 1).setValue(table[i][0]);
  }
  return;
}

function __add_player(name, playersInTable) {
  var input = Browser.msgBox(name + ' ist noch nicht in der Rangliste aufgeführt. Willst du ' + name + " der Rangliste hinzufügen?",
                             Browser.Buttons.OK_CANCEL);
  if (input == 'cancel') {
    return input;
  }
  while (true) {
    if(addPlayer(name)) {
      playersInTable.push(name);
      return input;
    }
    else {
      input = Browser.msgBox("Nochmal versuchen?", Browser.Buttons.OK_CANCEL);
      if (input == 'cancel') {
        return input;
      }
    }
  }
}

function enterNewRound() {
  const tableSheet = SpreadsheetApp.getActive().getSheetByName('Tabelle');
  const tableLastRow = tableSheet.getLastRow();
  const rowFirstPlayer = determineRowFirstPlayer(tableSheet);
  var playersInTable = [];
  for (i = rowFirstPlayer; i <= tableLastRow; i++) {
    playersInTable.push(tableSheet.getRange(i, 2).getValue());
  }
  
  var results = [];
  var tmp;
  var playersWhoAlreadyPlayed = "";
  var input = "";
  Browser.msgBox('Gib die Resultate der einzelnen Matches ein.');
  while(input != 'cancel') {
    tmp = enterMatchResult();
    if (tmp) {
      if (playersWhoAlreadyPlayed.indexOf(tmp[0]) != -1) {
        Browser.msgBox('Fehler: ' + tmp[0] + ' hat in dieser Runde schon gespielt.');
      }
      else if (playersWhoAlreadyPlayed.indexOf(tmp[1]) != -1) {
        Browser.msgBox('Fehler: ' + tmp[1] + ' hat in dieser Runde schon gespielt.');
      }
      else {
        if (playersInTable.indexOf(tmp[0]) == -1) {
          input = __add_player(tmp[0], playersInTable);
        }
        if (input != 'cancel' && playersInTable.indexOf(tmp[1]) == -1) {
          input = __add_player(tmp[1], playersInTable);
        }
        if (input != 'cancel') {
          playersWhoAlreadyPlayed = playersWhoAlreadyPlayed.concat(tmp[0]).concat(tmp[1]);
          results.push(tmp);
        }
      }
    }
    input = Browser.msgBox('Noch ein Resultat eingeben?:', Browser.Buttons.OK_CANCEL);
  }
  
  if (results.length == 0) {
    return;
  }
  
  const resultsSheet = SpreadsheetApp.getActive().getSheetByName('Runden');
  var resultsLastRow = resultsSheet.getLastRow();
  var roundNumber;
  while(resultsLastRow > 0) {
    tmp = resultsSheet.getRange(resultsLastRow, 1).getValue()
    if (typeof tmp == "string" && tmp.indexOf("Runde") != -1) {
      roundNumber = parseInt(tmp.split(" ")[1]) + 1;
      break;
    }
    resultsLastRow -= 1;
  }
  if (!roundNumber) {
    roundNumber = 1;
  }
  
  resultsSheet.appendRow([" "]);
  resultsSheet.appendRow(["Runde ".concat(roundNumber.toString()), Utilities.formatDate(new Date(), "GMT", "dd.MM.yy")]);
  resultsSheet.getRange(resultsSheet.getLastRow(), 1).setFontWeight("bold");
  resultsSheet.appendRow(["Spieler 1", "Spieler 2", "Sieger", "Punkte f. Sieger"]);
  
  const numPlayers = tableSheet.getLastRow() - rowFirstPlayer + 1;
  var points;
  for(i = 0; i < results.length; i++) {
    tmp = tableSheet.getRange(rowFirstPlayer + playersInTable.indexOf(results[i][2]), 3);
    if (results[i][2] == results[i][0]) {
      points = 1 + numPlayers - tableSheet.getRange(rowFirstPlayer + playersInTable.indexOf(results[i][1]), 1).getValue();
      tmp.setValue(tmp.getValue() + points);
      resultsSheet.appendRow(results[i].concat(points));
    }
    else {
      points = 1 + numPlayers - tableSheet.getRange(rowFirstPlayer + playersInTable.indexOf(results[i][0]), 1).getValue();
      tmp.setValue(tmp.getValue() + points);
      resultsSheet.appendRow(results[i].concat(points));
    }
    resultsSheet.getRange(resultsSheet.getLastRow(), 1, 4).setFontWeight("normal");
  }
  updateTable(tableSheet, rowFirstPlayer);
}


function enterMatchResult() {
  var input = Browser.inputBox('Name Spieler 1:', Browser.Buttons.OK_CANCEL);
  if (input == 'cancel') {
    return;
  }
  const player1 = String(input);
  input = Browser.inputBox('Name Spieler 2:', Browser.Buttons.OK_CANCEL);
  if (input == 'cancel') {
    return;
  }
  const player2 = String(input);
  
  input = Browser.inputBox('Sieger (' + player1 + ' oder ' + player2 + '):', Browser.Buttons.OK_CANCEL);
  if (input == 'cancel') {
    return;
  }
  const victor = String(input);
  if (victor != player1 && victor != player2){
    Browser.msgBox('Fehler: Siegername nicht als Spielername aufgeführt.');
    return;
  }
  return [player1, player2, victor]
}